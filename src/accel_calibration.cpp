/*
 * This file contains logic for calibrating an accelerometer. It works by taking
 * averages of readings from 6 sides in order to solve for the offset and scale
 * of each axis.
 *
 * @author Kalyan Sriram <kalyan@coderkalyan.com>
 */

#include <cstdio>
#include <zephyr.h>
#include <logging/log.h>

#include <matrix/math.hpp>

#include <messenger/messenger.h>
#include <messenger/accel_raw.h>
#include <lowpassfilter.h>

#include "accel_calibration.h"

using namespace matrix;

LOG_MODULE_REGISTER(accel_calibration, LOG_LEVEL_DBG);

static messenger::Subscriber<accel_raw_s> accel_sub(0);
static constexpr int accel_samples = 250;
static Vector3f offset;
static Matrix3f scale;
static bool calibrated_sides[6];

LowPassFilter<Vector3f> filter(50);

static CalibrationSide detect_side(void)
{
	// wait for the IMU to be still on a certain side for > 3 seconds,
	// then return that side.

	accel_raw_s accel_raw;

	int64_t clk = k_ticks_to_us_ceil64(k_uptime_ticks());
	while (true) {
		if (accel_sub.updated()) {
			accel_raw = accel_sub.get();
		} else {
			clk += 10000; /* wait 10 milliseconds before trying again */
			k_sleep(K_TIMEOUT_ABS_US(clk));
			continue;
		}

		unsigned counter = 0;
		
		// BACK_DOWN [ g, 0, 0]
		if ((accel_raw.x > DETECTION_THRESHOLD) && !calibrated_sides[0]) {
			while (accel_raw.x > DETECTION_THRESHOLD) {
				counter++;
				if (counter > 300) {
					printf("Detected BACK_DOWN\n");
					return CalibrationSide::BACK_DOWN;
				}

				clk += 10000; /* 100Hz */
				k_sleep(K_TIMEOUT_ABS_US(clk));
				accel_raw = accel_sub.get();
			}
		}
		// FRONT_DOWN [ -g, 0, 0]
		else if ((accel_raw.x < -DETECTION_THRESHOLD) && !calibrated_sides[1]) {
			while (accel_raw.x < -DETECTION_THRESHOLD) {
				counter++;
				if (counter > 300) {
					printf("Detected FRONT_DOWN\n");
					return CalibrationSide::FRONT_DOWN;
				}

				clk += 10000; /* 100Hz */
				k_sleep(K_TIMEOUT_ABS_US(clk));
				accel_raw = accel_sub.get();
			}
		}
		// LEFT_DOWN [ 0, g, 0]
		else if ((accel_raw.y > DETECTION_THRESHOLD) && !calibrated_sides[2]) {
			while (accel_raw.y > DETECTION_THRESHOLD) {
				counter++;
				if (counter > 300) {
					printf("Detected LEFT_DOWN\n");
					return CalibrationSide::LEFT_DOWN;
				}

				clk += 10000; /* 100Hz */
				k_sleep(K_TIMEOUT_ABS_US(clk));
				accel_raw = accel_sub.get();
			}
		}
		// RIGHT_DOWN [ 0, -g, 0]
		else if ((accel_raw.y < -DETECTION_THRESHOLD) && !calibrated_sides[3]) {
			while (accel_raw.y < -DETECTION_THRESHOLD) {
				counter++;
				if (counter > 300) {
					printf("Detected RIGHT_DOWN\n");
					return CalibrationSide::RIGHT_DOWN;
				}

				clk += 10000; /* 100Hz */
				k_sleep(K_TIMEOUT_ABS_US(clk));
				accel_raw = accel_sub.get();
			}
		}
		// BOTTOM_DOWN [ 0, 0, g]
		else if ((accel_raw.z > DETECTION_THRESHOLD) && !calibrated_sides[4]) {
			while (accel_raw.z > DETECTION_THRESHOLD) {
				counter++;
				if (counter > 300) {
					printf("Detected BOTTOM_DOWN\n");
					return CalibrationSide::BOTTOM_DOWN;
				}

				clk += 10000; /* 100Hz */
				k_sleep(K_TIMEOUT_ABS_US(clk));
				accel_raw = accel_sub.get();
			}
		}
		// TOP_DOWN [ 0, 0, -g]
		else if ((accel_raw.z < -DETECTION_THRESHOLD) && !calibrated_sides[5]) {
			while (accel_raw.z < -DETECTION_THRESHOLD) {
				counter++;
				if (counter > 300) {
					printf("Detected TOP_DOWN\n");
					return CalibrationSide::TOP_DOWN;
				}

				clk += 10000; /* 100Hz */
				k_sleep(K_TIMEOUT_ABS_US(clk));
				accel_raw = accel_sub.get();
			}
		}

		clk += 250000; /* 4Hz side detection */
		k_sleep(K_TIMEOUT_ABS_US(clk));
	}
}

static Vector3f calibrate_side(CalibrationSide side)
{
	filter.reset(Vector3f(0, 0, 0));

	const int accel_samples = 150;

	Vector3f average(0, 0, 0);

	int counter = 0;
	int64_t clk = k_ticks_to_us_ceil64(k_uptime_ticks());
	for (int i = 0;i < accel_samples;i++) {
		if (accel_sub.updated()) {
			accel_raw_s accel_raw = accel_sub.get();
			Vector3f accel_v = Vector3f(accel_raw.x, accel_raw.y, accel_raw.z);

			average += accel_v;
			counter++;
		}

		clk += 20000; /* 50Hz */
		k_sleep(K_TIMEOUT_ABS_US(clk));
	}

	average /= counter;

	printf("%f %f %f\n", average(0), average(1), average(2));
	return average;
}

static void calibrate_accel(struct k_work *work)
{
	ARG_UNUSED(work);

	// 6 sides to sample
	
	Vector3f sample_back_down {};
	Vector3f sample_front_down {};
	Vector3f sample_left_down {};
	Vector3f sample_right_down {};
	Vector3f sample_bottom_down {};
	Vector3f sample_top_down {};

	std::fill(calibrated_sides, calibrated_sides + 6, false);

	for (int i = 0;i < 6;i++) {
		CalibrationSide side = detect_side();

		switch (side) {
		case CalibrationSide::BACK_DOWN:
			sample_back_down = calibrate_side(side);
			calibrated_sides[0] = true;
			break;
		case CalibrationSide::FRONT_DOWN:
			sample_front_down = calibrate_side(side);
			calibrated_sides[1] = true;
			break;
		case CalibrationSide::LEFT_DOWN:
			sample_left_down = calibrate_side(side);
			calibrated_sides[2] = true;
			break;
		case CalibrationSide::RIGHT_DOWN:
			sample_right_down = calibrate_side(side);
			calibrated_sides[3] = true;
			break;
		case CalibrationSide::BOTTOM_DOWN:
			sample_bottom_down = calibrate_side(side);
			calibrated_sides[4] = true;
			break;
		case CalibrationSide::TOP_DOWN:
			sample_top_down = calibrate_side(side);
			calibrated_sides[5] = true;
			break;
		};
	}

	// calculate offset and scale
	// X offset: average X from BACK_DOWN + FRONT_DOWN
	offset(0) = (sample_back_down(0) + sample_front_down(0)) * 0.5f;

	// Y offset: average Y from LEFT_DOWN + RIGHT_DOWN
	offset(1) = (sample_left_down(1) + sample_right_down(1)) * 0.5f;
	
	// Z offset: average Z from BOTTOM_DOWN + FRONT_DOWN
	offset(2) = (sample_bottom_down(2) + sample_top_down(2)) * 0.5f;

	// transform matrix
	scale.setZero();
	printf("%f %f %f %f %f %f\n", sample_back_down(0), sample_front_down(0),
			sample_left_down(1), sample_right_down(1),
			sample_bottom_down(2), sample_top_down(2));
	scale(0, 0) = 2 * GRAVITY_ACCEL / (sample_back_down(0) - sample_front_down(0));
	scale(1, 1) = 2 * GRAVITY_ACCEL / (sample_left_down(1) - sample_right_down(1));
	scale(2, 2) = 2 * GRAVITY_ACCEL / (sample_bottom_down(2) - sample_top_down(2));

	printf("offset: %f %f %f\n", offset(0), offset(1), offset(2));
	printf("scale: %f %f %f\n", scale(0, 0), scale(1, 1), scale(2, 2));
	//matrix::Matrix3f mat_A;
	//mat_A.row(0) = sample_back_down - _offset;
	//mat_A.row(1) = sample_left_down - _offset;
	//mat_A.row(2) = sample_bottom_down - _offset;

	// calculate inverse matrix for A: simplify matrices mult because b has only
	// one non-zero element == g at index i
	//const matrix::Matrix3f accel_T = mat_A.I() * GRAVITY_ACCEL;
	//_scale = accel_T;
}

K_WORK_DEFINE(accel_calibration_work, calibrate_accel);

void do_accel_calibration(void)
{
	k_work_submit(&accel_calibration_work);
}
