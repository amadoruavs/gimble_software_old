/*
 * estimator.cpp
 *
 * This file contains an implementation of a Mahony filter for determining
 * attitude from raw IMU measurements.
 *
 * @author Kalyan Sriram <kalyan@coderkalyan.com>
 */

#include <cstdio>
#include <cmath>
#include <logging/log.h>

#include <matrix/math.hpp>

#include <messenger/messenger.h>
#include <messenger/accel_raw.h>
#include <messenger/gyro_raw.h>

LOG_MODULE_REGISTER(estimator, LOG_LEVEL_DBG);

#define ESTIMATOR_STACK_SIZE 2048
#define ESTIMATOR_PRIORITY 0

using namespace matrix;

static messenger::Subscriber<accel_raw_s> accel_sub(0);
static messenger::Subscriber<gyro_raw_s> gyro_sub(0);

static Quaternion prev_estimate(1.0f, 0.0f, 0.0f, 0.0f);
static Vector3f integral;
static Vector3f proportional;

/* Run the calibration worker for each sensor
 * in order to find the values to put here.
 * TODO: implement register system for storing these values
 * automatically at runtime
 */
constexpr float GYRO_OFFSET_X =  0.018129;
constexpr float GYRO_OFFSET_Y = -0.031151;
constexpr float GYRO_OFFSET_Z = -0.010160;
const Vector3f gyro_offset(GYRO_OFFSET_X, GYRO_OFFSET_Y, GYRO_OFFSET_Z);

constexpr float ACCEL_OFFSET_X = -0.036332;
constexpr float ACCEL_OFFSET_Y =  0.345759;
constexpr float ACCEL_OFFSET_Z = -0.806313;
constexpr float ACCEL_SCALE_X  =  1.004384;
constexpr float ACCEL_SCALE_Y  =  1.004218;
constexpr float ACCEL_SCALE_Z  =  0.987451;
const Vector3f accel_offset(ACCEL_OFFSET_X, ACCEL_OFFSET_Y, ACCEL_OFFSET_Z);
constexpr float accel_scale_data[9] = {
		ACCEL_SCALE_X, 0.0f, 0.0f,
		0.0f, ACCEL_SCALE_Y, 0.0f,
		0.0f, 0.0f, ACCEL_SCALE_Z
};
const Matrix3f accel_scale(accel_scale_data);

constexpr float kp = 30.00f;
constexpr float ki = 5.00f;

static void estimator_thread(void *unused1, void *unused2, void *unused3)
{
	ARG_UNUSED(unused1);
	ARG_UNUSED(unused2);
	ARG_UNUSED(unused3);

	int64_t prev_time = 0;

	/* wait until the imu starts pushing data */
	while (!accel_sub.updated() || !gyro_sub.updated()) {
		accel_raw_s accel_raw = accel_sub.get();
		prev_time = accel_raw.timestamp;

		k_sleep(K_MSEC(10));
	}

	Eulerf angle(0.0f, 0.0f, 0.0f);

	int64_t clk = k_ticks_to_us_ceil64(k_uptime_ticks());
	while (true) {
		if (accel_sub.updated() && gyro_sub.updated()) {
			accel_raw_s accel_raw = accel_sub.get();
			gyro_raw_s gyro_raw = gyro_sub.get();

			Vector3f accel(accel_raw.x, accel_raw.y, accel_raw.z);
			Vector3f gyro(gyro_raw.x, gyro_raw.y, gyro_raw.z);

			/* apply calibration and normalize */
			gyro -= gyro_offset;
			accel = (accel_scale * accel) - accel_offset;
			accel.normalize();

			float dt = (accel_raw.timestamp - prev_time) / (float) USEC_PER_SEC;
			prev_time = accel_raw.timestamp;

			Quaternionf &q = prev_estimate;
			Vector3f gravity_vec =
				2.0f * Vector3f(q(1)*q(3) - q(0)*q(2),
								q(0)*q(1) + q(2)*q(3),
								q(0)*q(0) + q(3)*q(3) - 0.5f);
			Vector3f error = accel.cross(gravity_vec);
			integral += ki * dt * error;
			proportional = kp * error;

			/* no magnetometer, and accel is useless horizontally
			 * so cannot compensate for yaw drift. */
			integral(2) = 0;
			proportional(2) = 0;

			gyro = gyro + proportional + integral;
			Quaternionf roc;

			roc(0) = 0.5f * (-q(1)*gyro(0) - q(2)*gyro(1) - q(3)*gyro(2));
			roc(1) = 0.5f * (q(0)*gyro(0) + q(2)*gyro(2) - q(3)*gyro(1));
			roc(2) = 0.5f * (q(0)*gyro(1) - q(1)*gyro(2) + q(3)*gyro(0));
			roc(3) = 0.5f * (q(0)*gyro(2) + q(1)*gyro(1) - q(2)*gyro(0));

			Quaternionf estimate = prev_estimate + (roc * dt);
			estimate.normalize();
			prev_estimate = estimate;

			//angle += gyro * dt * 180.0 / M_PI;

			Eulerf ee = estimate;
			ee *= 180.f / M_PI;
			Eulerf accel_angle;
			accel_angle(0) = atan2f(accel(1), accel(2)) * 180.0 / M_PI;
			accel_angle(1) = atan2f(-accel(0), accel(2)) * 180.0 / M_PI;
			accel_angle(2) = atan2f(-accel(1), accel(0)) * 180.0 / M_PI;
			printf("%.2f %.2f %.2f %.2f %.2f %.2f\n", ee(0), ee(1), ee(2),
					accel_angle(0), accel_angle(1), accel_angle(2));
		}

		clk += 1000; /* 1Khz */
		k_sleep(K_TIMEOUT_ABS_US(clk));
	}
}

K_THREAD_DEFINE(estimator_thread_id, ESTIMATOR_STACK_SIZE,
				estimator_thread, NULL, NULL, NULL,
				ESTIMATOR_PRIORITY, 0, 0);
