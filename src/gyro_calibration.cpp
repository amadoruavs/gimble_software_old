/*
 * This file contains logic for calibrating a gyroscope. It works by taking a series
 * of measurements of each axis and averaging them to determine the offset.
 *
 * @author Kalyan Sriram <kalyan@coderkalyan.com>
 */

#include <cstdio>
#include <logging/log.h>
#include <zephyr.h>

#include <matrix/math.hpp>

#include <messenger/messenger.h>
#include <messenger/gyro_raw.h>

#include "gyro_calibration.h"

using namespace matrix;

LOG_MODULE_REGISTER(gyro_calibration, LOG_LEVEL_DBG);

static messenger::Subscriber<gyro_raw_s> gyro_sub(0);
static constexpr int gyro_samples = 750;
static Vector3f offset;

static void calibrate_gyro(struct k_work *work)
{
	ARG_UNUSED(work);

	int counter = 0;
	offset = Vector3f(0.0f, 0.0f, 0.0f);
	int64_t clk = k_uptime_get();

	for (int i = 0;i < gyro_samples;i++) {
		if (gyro_sub.updated()) {
			gyro_raw_s gyro_raw = gyro_sub.get();

			offset += Vector3f(gyro_raw.x, gyro_raw.y, gyro_raw.z);
			counter++;
		}

		clk += 10; /* 100Hz */
		k_sleep(K_TIMEOUT_ABS_MS(clk));
	}

	offset /= counter;

	printf("gyro offset: %f %f %f\n", offset(0), offset(1), offset(2));
}

K_WORK_DEFINE(gyro_calibration_work, calibrate_gyro);

void do_gyro_calibration(void)
{
	k_work_submit(&gyro_calibration_work);
}
