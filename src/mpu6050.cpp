/*
 * mpu6050.cpp
 *
 * This file contains code for interfacing with the Invensense MPU6050
 * inertial measurement unit.
 *
 * @author Kalyan Sriram <kalyan@coderkalyan.com>
 */

#include <zephyr.h>
#include <device.h>
#include <drivers/sensor.h>
#include <logging/log.h>

#include <messenger/messenger.h>
#include <messenger/accel_raw.h>
#include <messenger/gyro_raw.h>

LOG_MODULE_REGISTER(mpu6050, LOG_LEVEL_DBG);

#define MPU6050_STACK_SIZE 1024
#define MPU6050_PRIORITY 0

messenger::Publisher<accel_raw_s> accel_pub(0);
messenger::Publisher<gyro_raw_s> gyro_pub(0);

static int sample(const struct device *device)
{
	int64_t time_now; /* in microseconds */
	int64_t uptime_ticks;
	struct sensor_value accel[3];
	struct sensor_value gyro[3];

	/* we currently timestamp based on when the value was set here
	 * TODO: figure out if another form of timestamping is better */
	uptime_ticks = k_uptime_ticks();
	time_now = k_ticks_to_us_ceil64(uptime_ticks);

	int ret;

	ret = sensor_sample_fetch(device);
	if (ret != 0) goto end;

	ret = sensor_channel_get(device, SENSOR_CHAN_ACCEL_XYZ, accel);
	if (ret != 0) goto end;
	ret = sensor_channel_get(device, SENSOR_CHAN_GYRO_XYZ, gyro);
	if (ret != 0) goto end;


	accel_raw_s accel_raw;
	gyro_raw_s gyro_raw;

	accel_raw.timestamp = time_now;
	accel_raw.x = sensor_value_to_double(&accel[1]);
	accel_raw.y = sensor_value_to_double(&accel[0]);
	accel_raw.z = sensor_value_to_double(&accel[2]);

	gyro_raw.timestamp = time_now;
	gyro_raw.x = sensor_value_to_double(&gyro[1]);
	gyro_raw.y = sensor_value_to_double(&gyro[0]);
	gyro_raw.z = sensor_value_to_double(&gyro[2]);

	accel_pub.publish(accel_raw);
	gyro_pub.publish(gyro_raw);

end:
	return ret;
}

static void mpu6050_thread(void *unused1, void *unused2, void *unused3)
{
	ARG_UNUSED(unused1);
	ARG_UNUSED(unused2);
	ARG_UNUSED(unused3);

	const struct device *device = device_get_binding(DT_LABEL(DT_ALIAS(imu_external)));
	if (device == NULL) {
		LOG_ERR("Unable to find device %s", DT_LABEL(DT_ALIAS(imu_external)));
		return;
	}

	int ret;

	int64_t clk = k_ticks_to_us_ceil64(k_uptime_ticks());

	while (true) {
		ret = sample(device);
		if (ret != 0) {
			LOG_ERR("Error sampling MPU6050: %d", ret);
		}

		clk += 1000; /* 1Khz */
		k_sleep(K_TIMEOUT_ABS_US(clk));
	}
}

K_THREAD_DEFINE(mpu6050_thread_id, MPU6050_STACK_SIZE, 
				mpu6050_thread, NULL, NULL, NULL,
				MPU6050_PRIORITY, 0, 0);
