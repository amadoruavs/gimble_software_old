/*
 * lowpassfilter.h
 *
 * This file contains a simple low pass filter, mainly used for filtering sensor
 * data in certain cases (like calibration) where the estimator's mahony filter
 * is not being used. The implementation is heavily based on Jacob Dahl's implementation
 * in TeensyFlight36
 * [https://github.com/dakejahl/TeensyFlight36/blob/develop/include/LowPassFilter.hpp]
 *
 * @author Kalyan Sriram <kalyan@coderkalyan.com>
 */

#pragma once

template <class T>
class LowPassFilter
{
public:
	LowPassFilter(float cutoff_freq) :
		_cutoff_freq(cutoff_freq)
	{
	}

	float apply(T input, int64_t timestamp)
	{
		double dt = (timestamp - _last_timestamp) / (double) USEC_PER_SEC;
		_last_timestamp = timestamp;

		T RC = 1.0 / (_cutoff_freq * 2 * M_PI);
		T alpha = dt / (RC + dt);

		// filter the output
		T output = alpha * input + (1 - alpha) * _previous_output;
		_previous_output = output;

		return output;
	}

	void reset(T value)
	{
		_previous_output = value;
	}

private:
	float _cutoff_freq = 0;

	T _previous_output;
	int64_t _last_timestamp = 0;
};
