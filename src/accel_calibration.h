#pragma once

static constexpr float DETECTION_THRESHOLD = 8.9f;
static constexpr float GRAVITY_ACCEL = 9.80665f;

/* each name represents the side facing down */
enum class CalibrationSide
{
	FRONT_DOWN = 0,
	BACK_DOWN,
	RIGHT_DOWN,
	LEFT_DOWN,
	TOP_DOWN,
	BOTTOM_DOWN
};

void do_accel_calibration(void);
