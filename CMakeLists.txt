cmake_minimum_required(VERSION 3.13.1)

list(APPEND BOARD_ROOT ${CMAKE_SOURCE_DIR})
list(APPEND DTS_ROOT ${CMAKE_SOURCE_DIR})

find_package(Zephyr REQUIRED HINTS $ENV{ZEPHYR_BASE})
project(gimble)

target_include_directories(app PRIVATE src)
target_include_directories(app PRIVATE extern/include/matrix)

target_sources(app PRIVATE
	src/mpu6050.cpp
	src/gyro_calibration.cpp
	src/accel_calibration.cpp
	src/estimator.cpp
	src/main.cpp
)
